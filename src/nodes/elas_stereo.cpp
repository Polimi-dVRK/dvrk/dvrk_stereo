//
// Created by nearlab on 28/09/17.
//

#include <ros/ros.h>
#include <nodelet/loader.h>

int main(int argc, char** argv) {
    
    ros::init(argc, argv, "elas_stereo", ros::init_options::AnonymousName);
    ros::NodeHandle node;
    
    nodelet::Loader nodelet(false); // Don't bring up the manager ROS API
    nodelet::M_string remap(ros::names::getRemappings());
    nodelet::V_string nodelet_argv;
    
    nodelet.load("elas_stereo", "dvrk_stereo/elas_stereo", remap, nodelet_argv);
    
    ros::spin();
    
    return 0;
}
