//
// Created by nearlab on 27/09/17.
//

#include <memory>

#include <nodelet/nodelet.h>
#include <dynamic_reconfigure/server.h>

#include <cv_bridge/cv_bridge.h>
#include <stereo_msgs/DisparityImage.h>

#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>

#include <dvrk_common/ros/util.hpp>
#include <dvrk_common/ros/params.hpp>
#include <dvrk_common/camera/extended_stereo_camera.hpp>

#include "libelas/elas.h"
#include "dvrk_stereo/ElasConfig.h"

// TODO: ensure that names are correctly remapped. esp. image => *_rect

namespace dvrk_stereo
{
    
    class ELASNodelet: public nodelet::Nodelet
    {
    public:
        
        typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloud_t;
        
        void onInit() override
        {
            auto nh = getNodeHandle();
            auto private_nh = getPrivateNodeHandle();
            
            _reconfigure_server.setCallback(boost::bind(&ELASNodelet::onReconfigure, this, _1, _2));
            
            _camera = std::make_unique<dvrk::ExtendedStereoCamera>(nh);
    
            ros::SubscriberStatusCallback connect_cb = boost::bind(&ELASNodelet::onConnect, this);
            ros::SubscriberStatusCallback disconnect_cb = boost::bind(&ELASNodelet::onDisconnect, this);
            
            _disparity_pub = nh.advertise<stereo_msgs::DisparityImage>("disparity", 1, connect_cb, disconnect_cb);
            _depth_pub = nh.advertise<sensor_msgs::Image>("depth", 1, connect_cb, disconnect_cb);
            _point_cloud_pub = nh.advertise<PointCloud_t>("depth/points", 1, connect_cb, disconnect_cb);

            _stereo_params = Elas::parameters(Elas::MIDDLEBURY);
            _elas = std::make_unique<Elas>(_stereo_params);
            
            NODELET_INFO("ELAS Stereo ready ... ");
        }
        
        void onConnect()
        {
            if (!_camera->is_subscribed()) {
                ros::NodeHandle private_nh = getPrivateNodeHandle();
                
                const std::string image_mono = private_nh.param<std::string>("image_mono", "image_rect");
                const std::string left_image_mono_topic = ros::names::append("left", image_mono);
                const std::string right_image_mono_topic = ros::names::append("right", image_mono);
    
                const std::string image_colour = private_nh.param<std::string>("image_colour", "image_rect_color");
                const std::string left_image_colour_topic = ros::names::append("left", image_colour);
                const std::string right_image_colour_topic = ros::names::append("right", image_colour);
                
                _camera->subscribe(
                    left_image_mono_topic, right_image_mono_topic,
                    left_image_colour_topic, right_image_colour_topic,
                    &ELASNodelet::onImage, this
                );
            }
        }
        
        void onDisconnect()
        {
            if (_disparity_pub.getNumSubscribers() == 0 && _point_cloud_pub.getNumSubscribers() == 0) {
                _camera->unsubscribe();
            }
        }
        
        void onImage(
            const sensor_msgs::ImageConstPtr &left_mono,
            const sensor_msgs::ImageConstPtr &right_mono,
            const sensor_msgs::ImageConstPtr &left_rgb,
            const sensor_msgs::ImageConstPtr &right_rgb,
            const image_geometry::StereoCameraModel &camera_model
        )
        {
            cv::Mat left_img = cv_bridge::toCvShare(left_mono, sensor_msgs::image_encodings::MONO8)->image;
            cv::Mat left_rgb_img = cv_bridge::toCvShare(left_rgb, sensor_msgs::image_encodings::BGR8)->image;
            cv::Mat right_img = cv_bridge::toCvShare(right_mono, sensor_msgs::image_encodings::MONO8)->image;
            
            // If the images aren't continuous we won't be able to copy the data out without first cloning them.
            assert(left_img.isContinuous() && right_img.isContinuous());
            
            cv::Mat left_disparity = cv::Mat::zeros(left_img.size(), CV_32FC1);
            float *left_disparity_ptr = reinterpret_cast<float *>(left_disparity.data);
            
            cv::Mat right_disparity = cv::Mat::zeros(left_img.size(), CV_32FC1);
            float *right_disparity_ptr = reinterpret_cast<float *>(right_disparity.data);
    
            // cv::Mat::Step return a cv::MatStep object that cab be implicitly cast into a size_t
            // but not other types so we first need to grab it as a size_t before casting it.
            const size_t left_img_step = left_img.step;
            const int32_t dims[3] = {
                left_img.cols,
                left_img.rows, boost::numeric_cast<int32_t>(left_img_step)
            };
            
            _elas->process(left_img.data, right_img.data, left_disparity_ptr, right_disparity_ptr, dims);
            
            // Disparity Computed, Time to prepare the messages
            
            stereo_msgs::DisparityImagePtr disparity_msg = boost::make_shared<stereo_msgs::DisparityImage>();
            disparity_msg->header = left_mono->header;
            disparity_msg->min_disparity = _stereo_params.disp_min;
            disparity_msg->max_disparity = _stereo_params.disp_max;
            disparity_msg->f = boost::numeric_cast<float>(camera_model.right().fx());
            disparity_msg->T = boost::numeric_cast<float>(camera_model.baseline());
            
            cv_bridge::CvImage disparity_img(left_mono->header, sensor_msgs::image_encodings::TYPE_32FC1, left_disparity);
            disparity_msg->image = *disparity_img.toImageMsg();
                _disparity_pub.publish(disparity_msg);
        
            // Now we start working on the point cloud & depth images
            
            // The depth image uses the so-called OpenNI format where each pixel is a 16 bit unsigned integer
            // representing a distance in mm.
            cv_bridge::CvImage depth_msg;
            depth_msg.header = left_mono->header;
            depth_msg.encoding = sensor_msgs::image_encodings::TYPE_32FC1;
            depth_msg.image = cv::Mat(left_disparity.size(), CV_32FC1);
            depth_msg.image.setTo(std::numeric_limits<double>::quiet_NaN());
    
            // since the point cloud may not dense (it may have holes where the disparity is invalid) we do not know the
            // number of points ahead of time.
            // TODO: profile to see if a simple std::back_appender is actually slower
            const auto num_inliers = static_cast<size_t>(cv::countNonZero(left_disparity >= 0));
            if (num_inliers == 0) {
                ROS_WARN_THROTTLE(1.0/15.0, "Unable to reconstruct scene.");
            }
    
            PointCloud_t::Ptr point_cloud(new PointCloud_t());
            point_cloud->header = pcl_conversions::toPCL(left_mono->header);
            point_cloud->height = 1;
            point_cloud->width = boost::numeric_cast<uint32_t>(num_inliers);
            point_cloud->is_dense = false;
            point_cloud->points.resize(num_inliers);
    
            size_t vec_idx = 0;
            for (int row = 0; row < left_disparity.rows; ++row) {
                for (int col = 0; col < left_disparity.cols; ++col) {
                    const cv::Point2d image_point(static_cast<double>(col), static_cast<double>(row));
                    float disparity_value = left_disparity.at<float>(image_point);
    
                    // Any negative or 0 values are invalid. A disparity value of 0 would yield a depth map value of inf
                    // which is clearly wrong. To indicate this we simply leave the default NaN value in place.
                    if (disparity_value <= 0)
                        continue;
                
                    depth_msg.image.at<float>(row, col) =
                        boost::numeric_cast<float>(camera_model.getZ(disparity_value));
                        
                    cv::Point3d world_point;
                    camera_model.projectDisparityTo3d(image_point, disparity_value, world_point);
                    
                    const cv::Vec3b point_color = left_rgb_img.at<cv::Vec3b>(row, col);
            
                    point_cloud->points[vec_idx].x = static_cast<float>(world_point.x);
                    point_cloud->points[vec_idx].y = static_cast<float>(world_point.y);
                    point_cloud->points[vec_idx].z = static_cast<float>(world_point.z);
                    point_cloud->points[vec_idx].r = point_color[2];
                    point_cloud->points[vec_idx].g = point_color[1];
                    point_cloud->points[vec_idx].b = point_color[0];
            
                    ++vec_idx;
                }
            }
    
            _depth_pub.publish(depth_msg);
            _point_cloud_pub.publish(point_cloud);
        }
        
        void onReconfigure(const dvrk_stereo::ElasConfig& cfg, uint32_t) {
            ROS_INFO("New reconfigure request, updating configuration ...");
            
            Elas::parameters new_params(Elas::MIDDLEBURY);
            new_params.disp_min = cfg.disp_min;
            new_params.disp_max = cfg.disp_max;
            new_params.support_threshold = cfg.support_threshold;
            new_params.support_texture = cfg.support_texture;
            new_params.add_corners = cfg.add_corners;
            new_params.grid_size = cfg.grid_size;
            new_params.beta = cfg.beta;
            new_params.gamma = cfg.gamma;
            new_params.sigma = cfg.sigma;
            new_params.match_texture = cfg.match_texture;
            new_params.lr_threshold = cfg.lr_threshold;
            new_params.speckle_size = cfg.speckle_size;
            new_params.ipol_gap_width = cfg.ipol_gap_width;

            new_params.filter_median = (cfg.filter == 0) /* Filter Median Enum Value */;
            new_params.filter_adaptive_mean = (cfg.filter == 1) /* Filter Adaptive Mean Enum Value */;;
            new_params.postprocess_only_left = cfg.postprocess_only_left;
    
            _elas = std::make_unique<Elas>(new_params);
            _stereo_params = new_params;
        }
    
    
    private:
        
        dynamic_reconfigure::Server<dvrk_stereo::ElasConfig> _reconfigure_server;
    
        std::unique_ptr<dvrk::ExtendedStereoCamera> _camera;
        
        ros::Publisher _disparity_pub;
        ros::Publisher _point_cloud_pub;
        ros::Publisher _depth_pub;
        
        Elas::parameters _stereo_params;
        std::unique_ptr<Elas> _elas;
    };
    
}

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(dvrk_stereo::ELASNodelet, nodelet::Nodelet);
